/**
 ** REMEMBER:
 **
 ** This code PROBABLY ONLY accesses the ENTWICKLUNGSKOEFFIZIENTEN from the Lorene fields
 ** and PROBABLY not the PHYSICAL values. Therefore, "the code works" is not that real.
 ***/


#include "dump-fields.h"

using namespace Lorene;
using namespace std;

// global variables
string starin, starout, program_name;
bool read_from_stdin, write_to_stdout;

void print_help() {
	fprintf(stderr,
		"Usage: %s <star_in> <star_out>\n"
		"    where 'star_in' is the directory holding 'resu.d' or directly the\n"
		"                    path to 'resu.d', including the filename.\n"
		"                    If '-' is given, the contents of resu.d is expected on stdin.\n"
		"    and 'star_out'  is the name of the 'resu.d' output file (or again, directory)\n"
		"                    If '-' is given, content is outputted on stdout.\n",
	program_name.c_str());
	exit(EXIT_FAILURE);
}

void die(const string& message) {
	cerr << program_name << ": Error, " << message << endl;
	exit(EXIT_FAILURE);
}


/*************** Main program ***************/

int main(int argc, char** argv) {
	printf("LORENE binary file modifier.\n");
	program_name = argv[0];

	// read in command line arguments
	if(argc == 3) {
		string arg(argv[1]);
		if(arg == "-h" || arg == "--help" || arg == "-help")
			print_help();
		starin = string(argv[1]);
		starout = string(argv[2]);
	} else print_help();
	
	// check for files, find resu.d
	read_from_stdin = (starin == "-");
	if(!read_from_stdin && !path_exists(starin.c_str()))
		die(starin+" not accessible");
	if(!read_from_stdin && is_directory(starin.c_str())) {
		starin += "/resu.d";
		if(!path_exists(starin.c_str()))
			die(starin+string(" not accessible (already attached resu.d)"));
	}

	// check for output directory
	write_to_stdout = (starout == "-");
	if(!write_to_stdout && is_directory(starout.c_str()))
		starout += "/resu.d";
	if(path_exists(starout.c_str()))
		die(string("I will not overwrite the already existing file ")+starout);

	cerr << "Reading star from " << (read_from_stdin?"standard input":starin) << endl;
	cerr << "Writing modified star to " << (write_to_stdout?"standard output":starout) << endl;
	
	FILE* fich = read_from_stdin ? stdin : fopen(starin.c_str(), "r");
	FILE* fresu =  write_to_stdout ? stdout : fopen(starout.c_str(), "w") ;

	fprintf(stderr, "Reading in star... "); fflush(stderr);

	Mg3d spectral_grid(fich);
	cerr << "I have set up a Mg3d spectral_grid:" << endl << spectral_grid << endl;

	int nphi = spectral_grid.get_np(0) ;
	if (nphi < 4) {
		cerr << "Contructor of Mag_NS: " << endl ;
		cerr << "Fatal problem with initial data, "<< endl ;
		cerr << "the number of points in the phi direction is lower than 4."
		     << endl
		     << "Actually it value is " << nphi << "."
		     << endl ;
		cerr << "Impossible to rotate tensors to Cartesian triad. Please give as"
		     << endl ;
		cerr << "an input a result file from magstar with nphi >=4." << endl ;
		abort() ;
	}
	Map_et mapping(spectral_grid, fich) ;
	cerr << "I have set up a Map_et mapping:\n" << mapping << endl;

	Eos* p_eos = Eos::eos_from_file(fich) ;

	Et_rot_mag star(mapping, *p_eos, fich) ;

	star.equation_of_state() ;
	star.update_metric() ;
	star.extrinsic_curvature() ;
	star.hydro_euler() ;

	const Map& mp = star.get_mp();

	/*********   Changing the Particle density    ********/
	/*********   nbar (THC: dens)                 ********/
	bool change_nbar = true;
	if(change_nbar) {
		Tenseur ent = star.get_ent();
		Cmp ent_comp = ent.set();
		
		//Tenseur nbar = star.get_nbar();
		//Cmp nbar_comp = nbar.set();
	
		/* Awkward way to find out dimension lenghts */
		Scalar ent_scalar(ent());
		int l = 0; // Domain #0
		Dim_tbl dimensions = ent_scalar.domain(l).dim;
		assert(dimensions.ndim == 3); // this has to be a 3D scalar.
	
		// the three dimension lenghts, eg. [i,j,k]=[r,theta,phi]=[17,9,4]
		int* len = dimensions.dim;
		int iMax=len[0], jMax=len[1], kMax=len[2];
		/**
	    	l 	[input] domain index
	    	k 	[input] $\phi$ index
	    	j 	[input] $\theta$ index
	    	i 	[input] r ($\xi$) index 
	    	*/
		printf("i[r] = 0..%d, j[theta]=0..%d, k[phi]=0..%d, l[domain]:=1\n", iMax, jMax, jMax);
		
		double ent_c = ent()(0,0,0,0); /* c^2 */
	
		// create some artificial completely unphysical shell around the data
		double amp = 0.5 * ent_c;
		double mu = 2./3. * iMax;
		double width = 1./4. * iMax;
		
		
		for(int i=0; i<iMax; i++) {
			for(int j=0; j<jMax; j++) {
				for(int k=0; k<kMax; k++) {
					ent_comp.set(l,k,j,i) += amp * exp(- (i-mu)*(i-mu)/width);
				}
			}
		}
	
		// save the stuff back.
		//nbar.set() = nbar_comp;
		
		star.set_enthalpy(ent_comp);
		
		// only setters on star:
		// star.set_mp() = Map&
		// star.set_enthalpy(const Cmp&)
	}

	star.get_mp().get_mg()->sauve(fresu) ;	    // writing of the grid
	star.get_mp().sauve(fresu) ;                // writing of the mapping
	star.get_eos().sauve(fresu) ;  		    // writing of the EOS
	star.sauve(fresu) ;                         // writing of the star
	
	fprintf(stderr, "Finished\n");
}
