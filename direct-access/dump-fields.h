#ifndef _DUMP_FIELDS_INC_
#define _DUMP_FIELDS_INC_

/* For the helpers */
#include <cstdlib>
#include <time.h>
#include <sys/utsname.h> // gethostname etc.
#include <unistd.h> // file_exists etc.
#include <libgen.h> // basename, dirname
#include <sys/types.h>
#include <sys/stat.h>
#include <string>
#include <iostream>
#include <sstream>

// Lorene headers
#include "tenseur.h"
#include "et_rot_mag.h"
#include "eos.h"
#include "unites.h"
#include "metric.h"
#include "utilitaires.h"

void dump_scalar(FILE* file, const Lorene::Scalar& scalar, const Lorene::Map_et& map);
void dump_scalar(const char* filename, const Lorene::Scalar& scalar, const Lorene::Map_et& map, const char* name);
void dump_vector(const char* filename_template, const Lorene::Tenseur& tensor, const Lorene::Map_et& map, const char* name_template);
void dump_matrix(const char* filename_template, const Lorene::Tenseur& tensor, const char* name_template);

/* C++ frontend */
inline void dump_scalar(const std::string& filename, const Lorene::Scalar& scalar, const Lorene::Map_et& map, const std::string& name) {
	dump_scalar(filename.c_str(), scalar, map, name.c_str());
}
inline void dump_vector(const std::string& filename, const Lorene::Tenseur& tensor, const Lorene::Map_et& map, const std::string& name) {
	dump_vector(filename.c_str(), tensor, map, name.c_str());
}
inline void dump_matrix(const std::string& filename, const Lorene::Tenseur& tensor, const std::string& name) {
	dump_matrix(filename.c_str(), tensor, name.c_str());
}



/*************** Mini helper functions for file access and error output ***************/

// http://stackoverflow.com/a/4553076
inline int is_regular_file(const char *path) {
    struct stat path_stat;
    stat(path, &path_stat);
    return S_ISREG(path_stat.st_mode);
}

// http://stackoverflow.com/a/4553053
// stat also follows symlinks: http://stackoverflow.com/a/2635948
inline int is_directory(const char *path) {
   struct stat statbuf;
   if (stat(path, &statbuf) != 0)
       return 0;
   return S_ISDIR(statbuf.st_mode);
}

// file exists
inline bool path_exists(const char *path) {
    return ( access( path, F_OK ) != -1 );
}

// a C++ polyfill. This does not really work: The constant
// argument is modified anyway. Problematic when used with
// string foo("/..."); dirname(foo.c_str()) -> foo can have changed.
inline char* dirname(const char* vpath) {
	char* path = const_cast<char*>(vpath);
	return dirname(path);
}

// The dirname C++ adapter
inline char* dirname(const string& path) {
	const std::string::size_type size = path.size();
	char *buffer = new char[size + 1];   //we need extra char for NUL
	memcpy(buffer, path.c_str(), size + 1);
	char* res = dirname(buffer);
	return res;
	// This leaks memory on purpose. C's dirname is strange.
	//std::free(buffer);
}





#endif /* _DUMP_FIELDS_INC_ */
